function romanNumeralGenerator (int) {
	
	// if value outside limits, return an error
	if (int < 1 || int > 3999) { return 'Oops! The Roman Numeral Generator can only convert values between 1-3999.'; }
	
	// set up conversion object
	var a = {
		'M': {
			'div': 1000,		// divider
			'rep': true			// repetition allowed
		},
		'D': {
			'div': 500,
			'rep': false
		},
		'C': {
			'div': 100,
			'rep': true
		},
		'L': {
			'div': 50,
			'rep': false
		},
		'X': {
			'div': 10,
			'rep': true
		},
		'V': {
			'div': 5,
			'rep': false
		},
		'I': {
			'div': 1,
			'rep': true
		}
	};
	
	// pass number through each stage
	var order = new Array('M', 'D', 'C', 'L', 'X', 'V', 'I');
	var result = '';
	
	for (var i=0; i<order.length; i++) {
		var obj = a[order[i]];
		var num = int / obj.div;
		// is numeral applicable in this case?
		if (num >= 1) {
			// are we allowed to repeat?
			if (obj.rep) {
				// can only repeat 3 times
				var max = 4*obj.div;
				// is number greater than 3 times the divider?
				if (int >= max) {
					// append the current and previous numerals to the result and decrement by the difference
					result += order[i]+order[i-1];
					int -= (a[order[i-1]].div-a[order[i]].div);
				} else {
					// recursively append the numeral to the result and decrement the value
					for (var j=int; j>=obj.div; j-=obj.div) {
						result += order[i];
						int -= a[order[i]].div;
					}
				}
			} else {
				// is number greater than 3 times the divider?
				if (int-obj.div >= (4*a[order[i+1]].div)) {
					// append the next and previous numerals to the result and decrement by the difference
					result += order[i+1]+order[i-1];
					int -= (a[order[i-1]].div-a[order[i+1]].div);
				} else {
					// append the numeral to the result and decrement the value
					result += order[i];
					int -= a[order[i]].div;
				}
			}
		}
	}

	return result;
}