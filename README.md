How to Use
==========

	- Include the generator.js file
	- Call the romanNumeralGenerator function and pass it your integer
	- Use the result as you see fit!

Limitations
===========

	- Can only generate for values between 1-3999
